package com.epam.training.exceptions;

/**
 * @author Aleksandr_Mishin on 10/3/2017.
 */
public class Example01 {
    public static int getSomeInt() {
        try {
            double a = 1.0 / 0;
            return 1;
        } catch(ArithmeticException e) {
            return 0;
        } finally {
            return -1;
        }
    }

    public static void main(String[] args) {
        System.out.println(getSomeInt());
    }
}
