package com.epam.training.generics.ex02.shapes;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 */
public interface Shape {
    void draw();
}
