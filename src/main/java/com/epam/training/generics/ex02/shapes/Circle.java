package com.epam.training.generics.ex02.shapes;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 */
public class Circle implements Shape {
    public void draw() {
        System.out.println("it's a circle");
    }
}
