package com.epam.training.generics.ex02;

import com.epam.training.generics.ex02.shapes.Circle;
import com.epam.training.generics.ex02.shapes.Shape;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 */
public class BoundedWildcard {
    /*
        List<Shape> => List<? extends Shape>
     */
    static void draw(List<Shape> shapes) {
        for (Shape shape : shapes) {
            shape.draw();
        }
    }

    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<>();
        List<Circle> circles = new ArrayList<>();

        draw(shapes);
        draw(circles);
    }
}
