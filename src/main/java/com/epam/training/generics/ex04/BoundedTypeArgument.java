package com.epam.training.generics.ex04;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 */
public class BoundedTypeArgument {

    /*
     static <FIRST extends SECOND, SECOND> void addAll(Collection<FIRST> c1, Collection<SECOND> c2)
     */
    static <M> void addAll(Collection<M> c1, Collection<M> c2) {
        for(M o : c1) {
            c2.add(o);
        }
    }

    public static void main(String[] args) {
        addAll(new ArrayList<Integer>(), new ArrayList<Integer>());
        addAll(new ArrayList<Integer>(), new ArrayList<Object>());
    }
}
