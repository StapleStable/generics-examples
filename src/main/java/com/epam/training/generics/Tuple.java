package com.epam.training.generics;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 * Just an example of generic class.
 */
public class Tuple<T, E> {
    private T elementOne;
    private E elementTwo;

    public Tuple(T elementOne, E elementTwo) {
        this.elementOne = elementOne;
        this.elementTwo = elementTwo;
    }

    public T getElementOne() {
        return elementOne;
    }

    public void setElementOne(T elementOne) {
        this.elementOne = elementOne;
    }

    public E getElementTwo() {
        return elementTwo;
    }

    public void setElementTwo(E elementTwo) {
        this.elementTwo = elementTwo;
    }

    public void print() {
        System.out.println(elementOne.toString() + " " + elementTwo.toString());
    }

    public static void main(String[] args) {
        Tuple<String, String> tuple = new Tuple<>("one", "two");
        Tuple<Integer, Double> tuple2 = new Tuple<>(1, 42.0);
        tuple.print();
        System.out.println();
        tuple2.print();
    }
}
