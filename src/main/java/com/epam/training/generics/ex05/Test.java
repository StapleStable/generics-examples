package com.epam.training.generics.ex05;

import java.util.Arrays;
import java.util.List;

/**
 * @author Aleksandr_Mishin on 10/3/2017.
 */
public class Test implements Comparable<Object> {

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    /*
    This is the only line to be changed.
    Use super keyword.
    Wisely.
     */
    private static <T extends Comparable<T>> T max(List<T> list) {
        T max = list.get(0);
        for(T element : list) {
            if(element.compareTo(max) > 0) {
                max = element;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
        System.out.println(max(list));


        Test test1 = new Test();
        Test test2 = new Test();
        List<Test> tests = Arrays.asList(test1, test2);
        max(tests);
    }
}
