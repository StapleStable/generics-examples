package com.epam.training.generics.ex03;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 */
public class GenericMethod {
    /*
     private static <T> void addAll(T[] a, Collection<T> c)
     */
    private static void addAll(Object[] a, Collection<Object> c) {
        for(Object element : a) {
            c.add(element);
        }
    }

    public static void main(String[] args) {
        addAll(new String[10], new ArrayList<String>());
        addAll(new Object[10], new ArrayList<Object>());
        addAll(new String[10], new ArrayList<Object>());
    }
}
