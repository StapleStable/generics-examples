package com.epam.training.generics.ex01;

/**
 * @author Aleksandr_Mishin on 10/3/2017.
 */
public class Mark<T extends Number> {
    private T mark;

    public Mark(T mark) {
        this.mark = mark;
    }


    /*
        Для компиляции в методах заменить Mark<T> на Mark<?>
     */
    public boolean isSame(Mark<T> mark) {
        return this.getMark().equals(mark.getMark());
    }

    public boolean isSameRounded(Mark<T> mark) {
        return this.roundMark() == mark.roundMark();
    }

    public T getMark() {
        return mark;
    }

    public int roundMark () {
        return Math.round(mark.floatValue());
    }


    public static void main(String[] args) {
        Mark<Integer> mark1 = new Mark<>(new Integer(42));
        Mark<Double> mark2 = new Mark<>(new Double(42.1));

        System.out.println(mark1.isSameRounded(mark2));
    }
}
