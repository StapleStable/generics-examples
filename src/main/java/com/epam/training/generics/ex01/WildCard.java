package com.epam.training.generics.ex01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Aleksandr_Mishin on 10/2/2017.
 */
public class WildCard {
    /*
        Collection<Object> => Collection<?>
     */
    private static void dump(Collection<Object> collection) {
        for (Object o : collection) {
            System.out.println(o);
        }
    }

    public static void main(String[] args) {
        List<Object> objects = new ArrayList<>();
//        dump(objects);
        List<Integer> collection = Arrays.asList(1, 2, 3);
        dump(collection);
    }


}
